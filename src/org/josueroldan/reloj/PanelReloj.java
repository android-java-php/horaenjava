
package org.josueroldan.reloj;

/**
 *
 * @author Josue Daniel Roldan Ochoa
 */



import java.text.SimpleDateFormat;
import java.util.Date;


public class PanelReloj extends javax.swing.JFrame {

 
    public PanelReloj() {
        this.setLocationRelativeTo(null);
        initComponents();
    relojdate();
    txdate2.setText(getFechaActual());
    txan.setText(getaño());
    txhorass.setText(getHoraActual());
    txmes.setText(getMes());
    txdia.setText(getDia());
    }

   public void relojdate(){
   
   Date date= new Date();
    
 txdate.setText(date.toString());
   }
   
   public static String getFechaActual() {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
        return formateador.format(ahora);
    }
   
      public static String getaño() {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy");
        return formateador.format(ahora);
    }
      
      public static String getMes(){
      Date mes= new Date();
      SimpleDateFormat formato= new SimpleDateFormat("MM");
       
      return formato.format(mes);
      }
      
         public static String getDia(){
      Date mes= new Date();
      SimpleDateFormat formato= new SimpleDateFormat("dd");
       
      return formato.format(mes);
      }
   
    public static String getHoraActual() {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("hh:mm:ss");
        return formateador.format(ahora);
    }
   
   
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txdate = new javax.swing.JTextField();
        txdate2 = new javax.swing.JTextField();
        txan = new javax.swing.JTextField();
        txhorass = new javax.swing.JTextField();
        txmes = new javax.swing.JTextField();
        txdia = new javax.swing.JTextField();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        txdate.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        txdate2.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        txan.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txanActionPerformed(evt);
            }
        });

        txhorass.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        txmes.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        txdia.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jMenu1.setText("Salir");
        jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu1MouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txhorass)
                    .addComponent(txmes)
                    .addComponent(txan)
                    .addComponent(txdate2)
                    .addComponent(txdate)
                    .addComponent(txdia, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE))
                .addContainerGap(23, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(txdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txdate2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txmes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txdia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txhorass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(24, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txanActionPerformed

    private void jMenu1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu1MouseClicked
System.exit(0);        // TODO add your handling code here:
    }//GEN-LAST:event_jMenu1MouseClicked

    
    
    
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PanelReloj.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PanelReloj.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PanelReloj.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PanelReloj.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PanelReloj().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JTextField txan;
    private javax.swing.JTextField txdate;
    private javax.swing.JTextField txdate2;
    private javax.swing.JTextField txdia;
    private javax.swing.JTextField txhorass;
    private javax.swing.JTextField txmes;
    // End of variables declaration//GEN-END:variables
}
